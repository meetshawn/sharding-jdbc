package com.demo.sharding.bean;

import lombok.Data;

@Data
public class UserBean {

    private long userId;
    private String fullname;
    private String userType;

}
