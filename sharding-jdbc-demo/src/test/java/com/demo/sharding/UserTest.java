package com.demo.sharding;

import com.demo.sharding.dao.OrderDAO;
import com.demo.sharding.dao.UserDAO;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShardingJDBCApplication.class})
public class UserTest {

    @Autowired
    UserDAO userDAO;
    @Autowired
    OrderDAO orderDAO;

    @Test
    public void testInsert() {
        for (int i = 0; i < 10; i++) {
            userDAO.insertOrder(i+1,"zs"+i,"1");
        }
    }

    @Test
    public void testSelectByIds(){
        //此时两个数都order_id均为奇数，故而只需要查t_order_2表
//        List<UserBean> maps = userDAO.selectByUserIds(Lists.newArrayList(1L, 9L));
        //order_id为1奇1偶，故而t_order_1和t_order_2均要查
        List<Map<String, Object>> maps = orderDAO.selectByOrderIds(Lists.newArrayList(590863728170237952L, 590863728606445569L));
        System.out.println(maps);
    }
}
